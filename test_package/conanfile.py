from conans import ConanFile, CMake, tools
import os
from os import path
from shutil import copyfile

username = os.getenv("CONAN_USERNAME", "noface")
channel = os.getenv("CONAN_CHANNEL", "testing")
package_name = "libnice"
version      = "0.1.13.1"

class PackageTest(ConanFile):
    settings = "os", "compiler", "arch"
    requires = "%s/%s@%s/%s" % (package_name, version, username, channel)
    generators = "cmake"
    default_options = ""

    def build(self):
        cmake = CMake(self.settings)
        cmake_config_cmd = 'cmake %s %s' % (self.conanfile_directory, cmake.command_line)

        self.output.info(cmake_config_cmd)
        self.run(cmake_config_cmd)
        self.run("cmake --build . %s" % (cmake.build_config))
                
    def test(self):
        exec_path = path.join('bin', 'example')
        self.output.info("running test: " + exec_path)
        self.run(exec_path)
