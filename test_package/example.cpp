#include <iostream>

#include <nice/agent.h>

#if GLIB_CHECK_VERSION(2, 36, 0)
#include <gio/gnetworking.h>
#endif

bool checkLibnice();

int main(void){

    std::cout << "************* Testing libnice ***************" << std::endl;    

    std::cout << (checkLibnice() ? "Ok" : "Error") << std::endl;

    std::cout << "***********************************************" << std::endl;   
    
    return 0;
}

// Simple code to check libnice was compiled correctly
bool checkLibnice() {
    bool ok=true;

    NiceAgent *agent;

#if GLIB_CHECK_VERSION(2, 36, 0)
    g_networking_init();
#else
    g_type_init();
#endif

    GMainLoop * gloop = g_main_loop_new(NULL, FALSE);

    // Create the nice agent
    agent = nice_agent_new(g_main_loop_get_context (gloop), NICE_COMPATIBILITY_RFC5245);
    if (agent == NULL){
        g_error("Failed to create agent");
        ok = false;
    }

    //Tear down
    g_main_loop_unref(gloop);
    g_object_unref(agent);

    return ok;
}

