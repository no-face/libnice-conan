from conans import ConanFile
import os
from os import path
from conans import tools
from conans.tools import download, unzip, check_sha256, pythonpath

class libniceConanFile(ConanFile):
    name        = "libnice"
    version     = "0.1.13.1"
    description = "Library for NAT traversal that implements ICE protocol."
    branch      = "stable"
    license     = "LGPL or MPL(Mozilla Public License)"
    url         = "https://gitlab.com/no-face/libnice-conan"  #recipe repo url
    lib_url     = "https://nice.freedesktop.org/"             #libnice site
    
    settings    = "os", "compiler", "arch"

    options = {
        "shared" : [True, False],
        "use_pic" : ["default", True, False],
    }
    default_options = (
        "shared=True",
        "use_pic=default",
    )
    
    BASE_URL_DOWNLOAD       = "https://nice.freedesktop.org/releases/"
    FILE_URL                = BASE_URL_DOWNLOAD + "libnice-0.1.13.tar.gz"
    EXTRACTED_FOLDER_NAME   = "libnice-0.1.13"
    FILE_SHA256             = '61112d9f3be933a827c8365f20551563953af6718057928f51f487bfe88419e1'

    requires = (
        "glib/2.48.2@noface/stable",
    )

    build_requires = "AutotoolsHelper/0.1.0@noface/testing"

    def configure(self):
        if self.options.use_pic == True and not self.options["glib"].shared:
            self.options["glib"].use_pic = True

    def source(self):
        zip_name = self.name + ".tar.gz"
        download(self.FILE_URL, zip_name)
        check_sha256(zip_name, self.FILE_SHA256)
        unzip(zip_name)

        os.remove(zip_name)
        
    def build(self):
        with tools.environment_append(self.make_pkg_config_env("glib")):
            self.prepare_build()
            self.configure_and_make()

    def package(self):
        # files are installed on build step
        pass

    def package_info(self):
        libs = ["nice"]

        self.cpp_info.libs = libs
        self.cpp_info.resdirs = ['share']

    ##################################################################################################
    
    def prepare_build(self):
        if getattr(self, "package_folder", None) is None:
            self.package_folder = path.abspath(path.join(".", "install"))
            self._try_make_dir(self.package_folder)
    
    def configure_and_make(self):

        with tools.chdir(self.EXTRACTED_FOLDER_NAME), pythonpath(self):
            from autotools_helper import Autotools

            autot = Autotools(self,
                shared      = self.options.shared)

            if self.options.use_pic != "default":
                autot.fpic = self.options.use_pic

            autot.configure()
            autot.build()
            autot.install()

    def make_pkg_config_env(self, dep_name, **args):
        deps = self.deps_cpp_info[dep_name]
        CFLAGS = " -I".join([""] + deps.include_paths)
        LIBS   = " -L".join([""] + deps.lib_paths)
        LIBS  += " -l".join([""] + args.get('libs', deps.libs))

        env = {}

        env[dep_name.upper() + '_CFLAGS'] = CFLAGS
        env[dep_name.upper() + '_LIBS']   = LIBS

        self.output.info("env for %s = %s" % (dep_name , str(env)))

        return env

    def _try_make_dir(self, d):
        try:
            os.mkdir(d)
        except OSError:
            #dir already exist
            pass
