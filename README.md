# libnice-conan

[Conan.io](https://www.conan.io/) package for [libnice](https://nice.freedesktop.org).

## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
[libnice documentation](https://nice.freedesktop.org/libnice/) for instruction in how to use the library.


## About libnice

> Libnice is an implementation of the IETF's Interactive Connectivity Establishment (ICE) standard (RFC 5245)
> and the Session Traversal Utilities for NAT (STUN) standard (RFC 5389).
>
> It provides a GLib-based library, libnice and a Glib-free library, libstun as well as GStreamer elements.
>
> ICE is useful for applications that want to establish peer-to-peer UDP data streams. It automates the process
> of traversing NATs and provides security against some attacks. It also allows applications to create reliable
> streams using a TCP over UDP layer.

Reference: [libnice wiki](https://nice.freedesktop.org/wiki/).


## License

This conan package is distributed under the [unlicense](http://unlicense.org/) terms (see LICENSE.md).

Libnice is distributed under both Mozilla Public License (MPL) version 1.1 and
GNU Lesser General Public License (LGPL) version 2.1.
See the license file at the main [repository](https://cgit.freedesktop.org/libnice/libnice/tree/COPYING)
or at mirrors: in [github](https://github.com/libnice/libnice/blob/master/COPYING)
or [gitlab](https://gitlab.com/libnice/libnice/blob/master/COPYING).


## Package limitations

This package does not offer all the options available at the build script.
Also, not all supported platforms may compile correctly yet.
So if you find problems, open an issue or send a pull-request.
